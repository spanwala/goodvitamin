<?php
namespace Magento\CatalogInventory\Model\Indexer\Stock\Processor;

/**
 * Interceptor class for @see \Magento\CatalogInventory\Model\Indexer\Stock\Processor
 */
class Interceptor extends \Magento\CatalogInventory\Model\Indexer\Stock\Processor implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Indexer\IndexerRegistry $indexerRegistry)
    {
        $this->___init();
        parent::__construct($indexerRegistry);
    }

    /**
     * {@inheritdoc}
     */
    public function getIndexer()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getIndexer');
        return $pluginInfo ? $this->___callPlugins('getIndexer', func_get_args(), $pluginInfo) : parent::getIndexer();
    }

    /**
     * {@inheritdoc}
     */
    public function reindexRow($id, $forceReindex = false)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'reindexRow');
        return $pluginInfo ? $this->___callPlugins('reindexRow', func_get_args(), $pluginInfo) : parent::reindexRow($id, $forceReindex);
    }

    /**
     * {@inheritdoc}
     */
    public function reindexList($ids, $forceReindex = false)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'reindexList');
        return $pluginInfo ? $this->___callPlugins('reindexList', func_get_args(), $pluginInfo) : parent::reindexList($ids, $forceReindex);
    }

    /**
     * {@inheritdoc}
     */
    public function reindexAll()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'reindexAll');
        return $pluginInfo ? $this->___callPlugins('reindexAll', func_get_args(), $pluginInfo) : parent::reindexAll();
    }

    /**
     * {@inheritdoc}
     */
    public function markIndexerAsInvalid()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'markIndexerAsInvalid');
        return $pluginInfo ? $this->___callPlugins('markIndexerAsInvalid', func_get_args(), $pluginInfo) : parent::markIndexerAsInvalid();
    }

    /**
     * {@inheritdoc}
     */
    public function getIndexerId()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getIndexerId');
        return $pluginInfo ? $this->___callPlugins('getIndexerId', func_get_args(), $pluginInfo) : parent::getIndexerId();
    }

    /**
     * {@inheritdoc}
     */
    public function isIndexerScheduled()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isIndexerScheduled');
        return $pluginInfo ? $this->___callPlugins('isIndexerScheduled', func_get_args(), $pluginInfo) : parent::isIndexerScheduled();
    }
}
