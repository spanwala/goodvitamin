echo "-----------------------------------";
echo " Clearing Cache ";
sudo rm -rf var/view_preprocessed/ pub/static/*
echo " Deployment start ";
sudo php bin/magento setup:static-content:deploy -f  -j 16
echo " Cache Flush ";
sudo php bin/magento cache:flush && sudo php bin/magento cache:clean && sudo chmod -R 777 generated/ var/ pub/
echo "-----------------------------------";
echo " Chown ";
sudo chown -R techsevin:techsevin *
echo "-----------------------------------";
echo " - Thankyou for using shell functionality -";
