<?php
namespace Magento\Framework\View\Asset\Config;

/**
 * Interceptor class for @see \Magento\Framework\View\Asset\Config
 */
class Interceptor extends \Magento\Framework\View\Asset\Config implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        $this->___init();
        parent::__construct($scopeConfig);
    }

    /**
     * {@inheritdoc}
     */
    public function isMergeCssFiles()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isMergeCssFiles');
        return $pluginInfo ? $this->___callPlugins('isMergeCssFiles', func_get_args(), $pluginInfo) : parent::isMergeCssFiles();
    }

    /**
     * {@inheritdoc}
     */
    public function isBundlingJsFiles()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isBundlingJsFiles');
        return $pluginInfo ? $this->___callPlugins('isBundlingJsFiles', func_get_args(), $pluginInfo) : parent::isBundlingJsFiles();
    }

    /**
     * {@inheritdoc}
     */
    public function isMergeJsFiles()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isMergeJsFiles');
        return $pluginInfo ? $this->___callPlugins('isMergeJsFiles', func_get_args(), $pluginInfo) : parent::isMergeJsFiles();
    }

    /**
     * {@inheritdoc}
     */
    public function isMinifyHtml()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isMinifyHtml');
        return $pluginInfo ? $this->___callPlugins('isMinifyHtml', func_get_args(), $pluginInfo) : parent::isMinifyHtml();
    }
}
