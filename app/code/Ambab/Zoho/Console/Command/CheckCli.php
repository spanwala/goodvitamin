<?php
namespace Ambab\Zoho\Console\Command;
use Magento\Framework\App\State as AppState;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
class CheckCli extends \Symfony\Component\Console\Command\Command
{
    protected $appState;
	protected $orderinterface;

    
    public function __construct(
        AppState $appState,
		\Magento\Sales\Model\ResourceModel\Sale\Collection $salesorder
    ) {
		$this->salesorder = $salesorder;
        $this->appState = $appState;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('zoho:sync') //  Your command name
            ->setDescription('This is Custom CLI command.'); // description for your command cli
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appState->setAreaCode('adminhtml');
	// Your logic here.
	$loadorder=$this->salesorder->addFieldToFilter('order_id',1000048231);
	print_r($loadorder->getData());
	$output-> writeln('Sync completed successfully');  // print message to terminal
    }
}