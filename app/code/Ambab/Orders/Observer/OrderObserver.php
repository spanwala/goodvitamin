<?php
namespace Ambab\Orders\Observer;
 
use Magento\Framework\Event\ObserverInterface;
 
class OrderObserver implements ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $Orderdata = $observer->getEvent()->getOrder();

        file_put_contents(BP.'/var/log/getdata.log', ' ::DATA:: '.print_r($Orderdata,true).PHP_EOL,FILE_APPEND);

        //Write your logic to create Json file withh the Order information
    }
}