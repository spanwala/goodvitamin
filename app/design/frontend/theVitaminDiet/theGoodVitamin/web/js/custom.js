 
require(['jquery'], function ($) {
	"use strict";
    //Your code here

	//Href anchoring
	$('.footer-links a').click(function(){
		$('html, body').animate({
			scrollTop: $( $(this).attr('href') ).offset().top
		}, 500);
		return false;
	});

    //alert('Here');
	//Text Circle
	$(".emblem").circleText({
		//glue: " ⭐ ⭐ ⭐ ",
		turn: true,
		padding: 0,
		radius: 100,
		duration: 40,
		repeat: 1,
		reverse: true
	  });


	return function () {
        console.log('JS Calling');
        // write your logic here.
    }
});