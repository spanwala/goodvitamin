Banner Extension for Magneto 2 developed by Sparsh

Welcome to Banner Slider Extension for Magneto 2 developed by Sparsh.

The extension lets the store owners place smart banners on their website and also helps in creating unlimited images and video banners for your website to reach the target audience.

##Support: 
version - 2.3.x, 2.4.x

##How to install Extension

1.	Get the extension’s version number
Note: Refer to the below link to get the version number of the downloaded extension.
https://devdocs.magento.com/extensions/install/#get-the-extensions-composer-name-and-version
2.	Navigate to your Magento project directory and update your composer.json file
Command: composer require sparsh/magento-2-banner-extension:<version>


#Enable Extension:
- php bin/magento module:enable Sparsh_Banner
- php bin/magento setup:upgrade
- php bin/magento setup:di:compile
- php bin/magento setup:static-content:deploy
- php bin/magento cache:flush

#Disable Extension:
- php bin/magento module:disable Sparsh_Banner
- php bin/magento setup:upgrade
- php bin/magento setup:di:compile
- php bin/magento setup:static-content:deploy
- php bin/magento cache:flush
