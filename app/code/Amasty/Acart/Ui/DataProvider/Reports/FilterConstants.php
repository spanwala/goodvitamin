<?php
declare(strict_types=1);

namespace Amasty\Acart\Ui\DataProvider\Reports;

class FilterConstants
{
    const ALL = 'all';
    const WEBSITE = 'website';
    const CAMPAIGNS = 'campaigns';
    const DATE_RANGE = 'date_range';
    const DATE_FROM = 'date_from';
    const DATE_TO = 'date_to';
}
